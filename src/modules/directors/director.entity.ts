import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Entity } from 'typeorm';
import { User } from '../users/user.entity';

@Entity()
@UseGuards(AuthGuard('Jwt'))
export abstract class Director extends User {}
