import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Matches, IsNotEmpty } from 'class-validator';
import { Entity, Column, ManyToMany } from 'typeorm';
import { BaseEntity } from '../../../utils/base-entities/base.entity.entity';
import { Gender } from '../../../utils/enums/gender.enum';
import * as bcrypt from 'bcryptjs';
import { Role } from '../roles/role.entity';

@Entity()
@UseGuards(AuthGuard('Jwt'))
export abstract class User extends BaseEntity {
  @Column({ type: 'varchar' })
  @ApiProperty({
    type: String,
  })
  name: string;

  @Column({ type: 'varchar', unique: false })
  @Matches(
    /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?=.*[0-9])(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    {
      message:
        'Passwords must contain at least: 8 characters in length and maximum 20 characters, 1 uppercase letter, 1 lowercase letter and 1 number',
    },
  )
  @IsNotEmpty()
  @ApiProperty({
    type: String,
  })
  @Exclude()
  password: string;

  @Column({ nullable: true })
  @Exclude()
  salt: string;

  @Column({ type: 'enum', enum: Gender })
  @ApiProperty({ type: Gender, enum: Gender })
  @IsNotEmpty()
  gender: Gender;

  @ManyToMany(() => Role, (roles) => roles.users)
  roles: Role[];

  async validationPassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }

  constructor(partial: Partial<User>) {
    super();
    Object.assign(this, partial);
  }
}
