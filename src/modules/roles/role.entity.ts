import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from '../../../utils/base-entities/base.entity.entity';
import { User } from '../users/user.entity';

@Entity()
export class Role extends BaseEntity {
  @Column({ type: 'enum', enum: Role })
  @ApiProperty({
    type: 'enum',
  })
  name: Role;

  @ManyToMany(() => User, (user) => user.roles)
  @JoinTable()
  users: User[];

  constructor(partial: Partial<Role>) {
    super();
    Object.assign(this, partial);
  }
}
