import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as ormconfig from '../../ormconfig';

export const typeOrmOptions: TypeOrmModuleOptions = ormconfig;
