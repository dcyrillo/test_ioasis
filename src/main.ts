import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('Ioasis challange')
    .setDescription('The Ioasis challange API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
  console.log(
    '\x1b[35m%s \x1b[0m%s \x1b[36m%s\x1b[0m',
    '[Ioasis Challange]',
    'Application is listening on port',
    3000,
  );
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());
}
bootstrap();
