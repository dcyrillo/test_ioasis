import * as dotenv from 'dotenv-flow';

dotenv.config();

const {
  POSTGRES_USER,
  POSTGRES_DB,
  DATABASE_TYPE,
  DATABASE_HOST,
  POSTGRES_PASSWORD,
  POSTGRES_PORT,
  DB_SYNCHRONIZE,
} = process.env;

module.exports = {
  type: DATABASE_TYPE,
  port: POSTGRES_PORT,
  host: DATABASE_HOST,
  username: POSTGRES_USER,
  password: POSTGRES_PASSWORD,
  database: POSTGRES_DB,
  entities: ['dist/**/*.entity{.ts,.js}'],
  migrationsTableName: 'migration',
  synchronize: DB_SYNCHRONIZE,
  logging: false,
  cli: {
    migrationsDir: 'src/migrations',
  },
  migrations: ['src/migrations/*.ts', 'src/migrations/*.js'],
};
