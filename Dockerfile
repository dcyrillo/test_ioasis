FROM node:lts-alpine AS development

ENV APP_FOLDER /home/node/app
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

RUN mkdir /tmp/users \
	&& chown -R node /tmp/users \
	&& mkdir $APP_FOLDER \
	&& chown -R node $APP_FOLDER

RUN apk update \
	&& apk upgrade \
	&& apk add --no-cache bash bash-completion nano xclip git

USER node

WORKDIR $APP_FOLDER

COPY --chown=node package*.json ./

RUN npm install nest -g 
RUN npm install && npm cache clean --force 

COPY --chown=node  . .

RUN npm run build

FROM postgres:12 AS postgres-pt_BR

RUN localedef -i pt_BR -c -f UTF-8 -A /usr/share/locale/locale.alias pt_BR.UTF-8

ENV LANG pt_BR.utf8