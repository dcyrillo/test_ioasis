#!/bin/sh

# ====================================================
# Generate generations
NICKNAME_MIGRATION="defina-um-nome" 		# Use kebab-case
# npm run typeorm:migrate $NICKNAME_MIGRATION
# ====================================================


# ====================================================
# Run migrations
# npm run typeorm:run
# ====================================================


# ====================================================
# Revert migrations
# npm run typeorm:revert
# ====================================================
